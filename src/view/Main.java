package view;


import controller.Controller;
import exception.ModelException;
import model.Expression;
import model.MyIStack;
import model.PrgState;
import model.Stmt;
import model.command.ExitCommand;
import model.command.RunExample;
import model.container.*;
import model.expression.*;
import model.statement.*;
import repository.MRepository;
import repository.Repository;


public class Main {
    private static Stmt prg1() {
        //a=5;
        //if (a-5) then
        //    print(1);
        //else
        //    print(2);
        //print((5+7)a);
        Stmt program = new CompStmt(
                new AssignStmt("a", new ConstExp(5)),
                new CompStmt(
                        new IfStmt(
                                new ArithExp(
                                        '-',
                                        new VarExp("a"),
                                        new ConstExp(5)
                                ),
                                new PrintStmt(new ConstExp(1)),
                                new PrintStmt(new ConstExp(2))
                        ),
                        new PrintStmt(
                                new ArithExp(
                                        '*',
                                        new ArithExp(
                                                '+',
                                                new ConstExp(5),
                                                new ConstExp(7)
                                        ),
                                        new VarExp("a")
                                )
                        )
                )
        );

        return program;
    }

    private static Stmt prg2() {
        /*
        a=2-2;
        If a Then
            v=2
        Else
            v=3;
        Print(v)
         */
        Stmt ex2 = new CompStmt(
                new AssignStmt("a",
                        new ArithExp('-',
                                new ConstExp(2),
                                new ConstExp(2))),
                new CompStmt(
                        new IfStmt(
                                new VarExp("a"),
                                new AssignStmt("v",
                                        new ConstExp(2)),
                                new AssignStmt("v",
                                        new ConstExp(3))),
                        new PrintStmt(
                                new VarExp("v"))));

        return ex2;
    }

    private static Stmt prg3() {
        /*
        a=2/0;
        if a then
            v=2
        else
            v=3
        print(v)
         */
        Stmt ex3 = new CompStmt(
                new AssignStmt("a",
                        new ArithExp('/',
                                new ConstExp(2),
                                new ConstExp(0))),
                new CompStmt(
                        new IfStmt(
                                new VarExp("a"),
                                new AssignStmt("v",
                                        new ConstExp(2)),
                                new AssignStmt("v",
                                        new ConstExp(3))),
                        new PrintStmt(
                                new VarExp("v"))));

        return ex3;
    }

    //file test
    private static Stmt prg4() {
    /*
    openRFile(var_f,"test.in");
    readFile(var_f,var_c);
    print(var_c);
    if var_c then
        readFile(var_f,var_c);
        print(var_c);
    else
        print(0);
    closeRFile(var_f);
     */
        Stmt ex4 = new CompStmt(
                new OpenFile("var_f", "test.in"),
                new CompStmt(
                        new ReadFile(
                                new VarExp("var_f"),
                                "var_c"),
                        new CompStmt(
                                new PrintStmt(new VarExp("var_c")),
                                new CompStmt(
                                        new IfStmt(
                                                new VarExp("var_c"),
                                                new CompStmt(
                                                        new ReadFile(new VarExp("var_f"), "var_c"),
                                                        new PrintStmt(new VarExp("var_c"))
                                                ),
                                                new PrintStmt(new ConstExp(0))
                                        ),
                                        new CloseFile(new VarExp("var_f"))
                                )
                        )
                )
        );
        return ex4;
    }

    //heap test
    private static Stmt prg5() {
        //v=10;
        //new(v,20);
        //new(a,22);
        //wH(a,30);
        //print(a);
        //print(rH(a));
        //a=0
        Stmt ex5 = new CompStmt(
                new CompStmt(
                        new CompStmt(
                                new AssignStmt("v", new ConstExp(10)),
                                new NewHeap("v", new ConstExp(20))
                        ),
                        new CompStmt(
                                new NewHeap("a", new ConstExp(22)),
                                new WriteHeap("a", new ConstExp(30))
                        )
                ),
                new CompStmt(
                        new CompStmt(
                                new PrintStmt(new VarExp("a")),
                                new PrintStmt(new ReadHeap("a"))
                        ),
                        new AssignStmt("a", new ConstExp(0))
                )
        );
        return ex5;
    }

    //files not closed test
    private static Stmt prg6() {
        //open(a,"a.txt")
        //open(b,"b.txt")
        //open(c,"c.txt")
        //open(d,"d.txt")
        //open(e,"e.txt")
        //open(f,"f.txt")
        //open(g,"g.txt")
        //read(g,h)
        Stmt ex5 = new CompStmt(
                new CompStmt(
                        new CompStmt(
                                new OpenFile("a", "a.txt"),
                                new OpenFile("b", "b.txt")
                        ),
                        new CompStmt(
                                new OpenFile("c", "c.txt"),
                                new OpenFile("d", "d.txt")
                        )
                ),
                new CompStmt(
                        new CompStmt(
                                new OpenFile("e", "e.txt"),
                                new OpenFile("f", "f.txt")
                        ),
                        new CompStmt(
                                new OpenFile("g", "g.txt"),
                                new ReadFile(new VarExp("g"), "h")
                        )
                )
        );
        return ex5;
    }
    //while test


    private static Stmt prg7() {
        // v=6;
        // (while (v!=4) print(v);v=v-1);
        // print(v)
        Stmt ex5 = new CompStmt(
                new CompStmt(
                        new AssignStmt("v",
                                new ConstExp(6)),
                        new WhileStmt(
                                new NotEqual(
                                        new VarExp("v"),
                                        new ConstExp(4)),
                                new CompStmt(
                                        new PrintStmt(new VarExp("v")),
                                        new AssignStmt("v",
                                                new ArithExp('-',
                                                        new VarExp("v"),
                                                        new ConstExp(1)))
                                ))
                ),
                new PrintStmt(new VarExp("v"))
        );
        return ex5;
    }

    private static Stmt prg8() {
        //  10 + (2<6) evaluates to 11
        Expression e = new ArithExp('+', new ConstExp(10), new Less(new ConstExp(2), new ConstExp(6)));
        try {
            System.out.println(e.eval(new MyMap<>(), new Heap<>()));
        } catch (ModelException e2) {

        }
        return null;
    }

    private static Stmt prg9(){
        //v=10;
        //new(a,22);
        //fork(wH(a,30);v=32;print(v);print(rH(a)));
        //print(v);
        //print(rH(a))

        Stmt prg9 = new CompStmt(
                new CompStmt(
                        new CompStmt(
                                new AssignStmt("v",new ConstExp(10)),
                                new NewHeap("a",new ConstExp(22))
                        ),
                        new ForkStmt(
                                new CompStmt(
                                    new CompStmt(
                                            new WriteHeap("a", new ConstExp(30)),
                                            new AssignStmt("v",new ConstExp(32))
                                    ),
                                    new CompStmt(
                                            new PrintStmt(new VarExp("v")),
                                            new PrintStmt(new ReadHeap("a"))
                                    )
                                )
                        )
                ),
                new CompStmt(
                        new PrintStmt(new VarExp("v")),
                        new PrintStmt(new ReadHeap("a"))
                )
        );
        return prg9;
    }

    public static void main(String[] args) {

        MyIStack<Stmt> stack1 = new MyStack<>();
        stack1.push(prg1());
        Repository repository1 = new MRepository(new PrgState(stack1, new MyMap<>(), new MyList<>(), new FileTable<>(), new Heap<>(), 1), "prg1.txt");
        Controller controller1 = new Controller(repository1);
        MyIStack<Stmt> stack2 = new MyStack<>();
        stack2.push(prg2());
        Repository repository2 = new MRepository(new PrgState(stack2, new MyMap<>(), new MyList<>(), new FileTable<>(), new Heap<>(), 1), "prg2.txt");
        Controller controller2 = new Controller(repository2);
        MyIStack<Stmt> stack3 = new MyStack<>();
        stack3.push(prg3());
        Repository repository3 = new MRepository(new PrgState(stack3, new MyMap<>(), new MyList<>(), new FileTable<>(), new Heap<>(), 1), "prg3.txt");
        Controller controller3 = new Controller(repository3);
        MyIStack<Stmt> stack4 = new MyStack<>();
        stack4.push(prg4());
        Repository repository4 = new MRepository(new PrgState(stack4, new MyMap<>(), new MyList<>(), new FileTable<>(), new Heap<>(), 1), "prg4.txt");
        Controller controller4 = new Controller(repository4);
        MyIStack<Stmt> stack5 = new MyStack<>();
        stack5.push(prg5());
        Repository repository5 = new MRepository(new PrgState(stack5, new MyMap<>(), new MyList<>(), new FileTable<>(), new Heap<>(), 1), "prg5.txt");
        Controller controller5 = new Controller(repository5);
        MyIStack<Stmt> stack6 = new MyStack<>();
        stack6.push(prg6());
        Repository repository6 = new MRepository(new PrgState(stack6, new MyMap<>(), new MyList<>(), new FileTable<>(), new Heap<>(), 1), "prg6.txt");
        Controller controller6 = new Controller(repository6);
        MyIStack<Stmt> stack7 = new MyStack<>();
        stack7.push(prg7());
        Repository repository7 = new MRepository(new PrgState(stack7, new MyMap<>(), new MyList<>(), new FileTable<>(), new Heap<>(), 1), "prg7.txt");
        Controller controller7 = new Controller(repository7);
        MyIStack<Stmt> stack9 = new MyStack<>();
        stack9.push(prg9());
        Repository repository9 = new MRepository(new PrgState(stack9, new MyMap<>(), new MyList<>(), new FileTable<>(), new Heap<>(), 1),"prg9.txt");
        Controller controller9 = new Controller(repository9);

        TextMenu menu = new TextMenu();
        menu.addCommand(new ExitCommand("0", "exit"));
        menu.addCommand(new RunExample("1",prg1()+"", controller1));
        menu.addCommand(new RunExample("2",prg2()+"", controller2));
        menu.addCommand(new RunExample("3",prg3()+"", controller3));
        menu.addCommand(new RunExample("4",prg4()+"", controller4));
        menu.addCommand(new RunExample("5",prg5()+"", controller5));
        menu.addCommand(new RunExample("6",prg6()+"", controller6));
        menu.addCommand(new RunExample("7",prg7()+"", controller7));
        menu.addCommand(new RunExample("9",prg9()+"", controller9));
        menu.show();

    }
}
