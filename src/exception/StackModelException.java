package exception;


public class StackModelException extends ModelException {
    public StackModelException(String message) {
        super(message);
    }
}
