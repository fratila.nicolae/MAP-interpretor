package exception;

public class ListModelException extends ModelException {
    public ListModelException(String message) {
        super(message);
    }
}
