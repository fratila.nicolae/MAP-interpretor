package exception;

public class MapModelException extends ModelException {
    public MapModelException(String message) {
        super(message);
    }
}
