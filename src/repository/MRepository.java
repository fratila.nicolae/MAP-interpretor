package repository;

import exception.MapModelException;
import exception.RepositoryException;
import exception.StackModelException;
import javafx.util.Pair;
import model.*;

import java.io.*;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MRepository implements Repository {
    private List<PrgState> list=new ArrayList<>();
    private String logFile;

    public MRepository(PrgState program, String logFile) {
        list.add(program);
        this.logFile=logFile;
    }

    public MRepository(String logFile) {
        this.logFile = logFile;
    }

    public void logProgStateExe(PrgState prgState) throws RepositoryException{
        try {
            PrintWriter f = new PrintWriter(new BufferedWriter(new FileWriter(logFile, true)));
            f.print("ID:");
            f.print(prgState.getId()+"\n");
            f.print("Exe Stack:\n");
            for (Stmt s : prgState.getExecStack().getAll()){
                f.print(s+"\n");
            }
            f.print("Sym Table:\n");
            MyIMap<String,Integer> st = prgState.getSymbolTable();
            for (String s : st.getKeys()){
                f.print(s+"-->"+st.get(s)+"\n");
            }
            f.print("Out:\n");
            for (Integer s : prgState.getOutput().getAll()){
                f.print(s+"\n");
            }
            f.print("File Table:\n");
            FileITable<Integer,Pair<String,BufferedReader>> fileTable = prgState.getFileTable();
            for (int i: fileTable.getAll()){
                f.print(i+"-->"+fileTable.find(i)+'\n');
            }
            f.print("Heap:\n");
            for (Map.Entry<Integer,Integer> entry : prgState.getHeap().getContent().entrySet()){
                f.print(entry.getKey()+"-->"+entry.getValue()+"\n");
            }
            f.print("=============================================\n");
            f.close();
        }
        catch (FileNotFoundException e){
            throw new RepositoryException("file not found");
            //TODO:exception handling
        }
        catch (MapModelException e){
            throw new RepositoryException("map exception");
            //TODO:exception handling
        }
        catch (IOException e){
            throw new RepositoryException("io exception"+e);
            //TODO:exception handling
        }
    }

    @Override
    public List<PrgState> getPrgList() {
        return list;
    }

    @Override
    public void setPrgList(List<PrgState> list) {
        this.list=list;
    }

    public void add(PrgState prgState) {
        list.add(prgState);
    }
}
