package repository;

import exception.RepositoryException;
import model.PrgState;

import java.util.List;

public interface Repository {

    List<PrgState> getPrgList();

    void setPrgList(List<PrgState> list);

    void add(PrgState prgState);

    public void logProgStateExe(PrgState prgState) throws RepositoryException;



}
