package model.expression;

import exception.ModelException;
import model.Expression;
import model.IHeap;
import model.MyIMap;

public class Mod10Exp implements Expression {
    private Expression expression;

    public Mod10Exp(Expression expression) {
        this.expression = expression;
    }

    @Override
    public Integer eval(MyIMap<String, Integer> symbolTable, IHeap<Integer, Integer> heap) throws ModelException {
        return this.expression.eval(symbolTable, heap) % 10;
    }

    @Override
    public String toString() {
        return expression + " MOD 10";
    }
}
