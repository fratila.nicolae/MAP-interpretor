package model.expression;

import exception.ModelException;
import model.Expression;
import model.IHeap;
import model.MyIMap;

public class ArithExp implements Expression {
    private char operation;
    private Expression left;
    private Expression right;

    public ArithExp(char operation, Expression left, Expression right) {
        this.operation = operation;
        this.left = left;
        this.right = right;
    }

    @Override
    public Integer eval(MyIMap<String, Integer> symbolTable, IHeap<Integer,Integer> heap) throws ModelException {
        switch (operation) {
            case '+': {
                return left.eval(symbolTable, heap) + right.eval(symbolTable,heap);
            }
            case '-': {
                return left.eval(symbolTable,heap) - right.eval(symbolTable,heap);
            }
            case '*': {
                return left.eval(symbolTable,heap) * right.eval(symbolTable,heap);
            }
            case '/': {
                Integer r = right.eval(symbolTable,heap);
                if (r != 0)
                    return left.eval(symbolTable,heap) / r;
                throw new ModelException("division by zero");
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return ""+left + operation + right;
    }
}
