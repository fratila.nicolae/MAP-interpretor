package model.expression;

import exception.ModelException;
import model.Expression;
import model.IHeap;
import model.MyIMap;

public class ReadHeap implements Expression {
    private String var;

    public ReadHeap(String var) {
        this.var = var;
    }

    @Override
    public Integer eval(MyIMap<String, Integer> symbolTable, IHeap<Integer, Integer> heap) throws ModelException {
        if (!symbolTable.contains(var))
            throw new ModelException("ReadHeap: variable does not exist");
        Integer value = symbolTable.get(var);
        if (!heap.contains(value))
            throw new ModelException("ReadHeap: illegal memory accession");
        return heap.get(value);
    }

    @Override
    public String toString() {
        return "rH(" + var + ")";
    }
}
