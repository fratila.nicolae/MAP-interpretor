package model.statement;

import exception.ModelException;
import model.Expression;
import model.PrgState;
import model.Stmt;

public class WhileStmt implements Stmt {
    private Expression condition;
    private Stmt statement;

    public WhileStmt(Expression condition, Stmt statement) {
        this.condition = condition;
        this.statement = statement;
    }

    @Override
    public PrgState execute(PrgState prgState) throws ModelException {
        if (!condition.eval(prgState.getSymbolTable(), prgState.getHeap()).equals(0)) {
            prgState.getExecStack().push(new WhileStmt(condition, statement));
            prgState.getExecStack().push(statement);
            return null;
        }
        return null;
    }

    @Override
    public String toString() {
        return "While(" + condition + "){" + statement + "}";
    }
}
