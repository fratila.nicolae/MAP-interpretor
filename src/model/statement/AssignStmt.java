package model.statement;

import exception.ModelException;
import model.Expression;
import model.MyIMap;
import model.PrgState;
import model.Stmt;

public class AssignStmt implements Stmt {
    private String var;
    private Expression exp;

    public AssignStmt(String var, Expression exp) {
        this.var = var;
        this.exp = exp;
    }

    @Override
    public PrgState execute(PrgState prgState) throws ModelException {
        MyIMap<String,Integer> m =prgState.getSymbolTable();
        m.put(var,exp.eval(m,prgState.getHeap()));
        return null;
    }

    @Override
    public String toString() {
        return var + '=' + exp+";";
    }


}

