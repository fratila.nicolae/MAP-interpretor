package model.statement;

import exception.ModelException;
import model.*;

public class WriteHeap implements Stmt {

    private String var_name;
    private Expression expr;

    public WriteHeap(String var_name, Expression expr) {
        this.var_name = var_name;
        this.expr = expr;
    }

    @Override
    public PrgState execute(PrgState prgState) throws ModelException {
        IHeap<Integer,Integer> heap = prgState.getHeap();
        MyIMap<String,Integer> systemTable = prgState.getSymbolTable();
        heap.update(systemTable.get(var_name),expr.eval(systemTable,heap));
        return null;
    }

    @Override
    public String toString() {
        return "wH(" +var_name+"," +expr + ");";
    }
}
