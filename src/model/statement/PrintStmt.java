package model.statement;

import exception.ModelException;
import model.Expression;
import model.PrgState;
import model.Stmt;

public class PrintStmt implements Stmt {
    private Expression exp;

    public PrintStmt(Expression exp) {
        this.exp = exp;
    }

    @Override
    public PrgState execute(PrgState prgState) throws ModelException {
        prgState.getOutput().add(exp.eval(prgState.getSymbolTable(),prgState.getHeap()));
        return null;
    }

    @Override
    public String toString() {
        return "Print("+ exp +");";
    }
}
