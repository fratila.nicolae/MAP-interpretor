package model;

import exception.ModelException;
import javafx.util.Pair;

import java.io.BufferedReader;

public class PrgState {

    private MyIStack<Stmt> execStack;
    private MyIMap<String, Integer> symbolTable;
    private MyIList<Integer> output;
    private FileITable<Integer, Pair<String, BufferedReader>> fileTable;
    private IHeap<Integer, Integer> heap;
    private Integer id;

    public PrgState(MyIStack<Stmt> execStack, MyIMap<String, Integer> symbolTable, MyIList<Integer> output, FileITable<Integer, Pair<String, BufferedReader>> fileTable, IHeap<Integer, Integer> heap, Integer id) {
        this.execStack = execStack;
        this.symbolTable = symbolTable;
        this.output = output;
        this.fileTable = fileTable;
        this.heap = heap;
        this.id = id;
    }

    public FileITable<Integer, Pair<String, BufferedReader>> getFileTable() {
        return fileTable;
    }

    public void setFileTable(FileITable<Integer, Pair<String, BufferedReader>> fileTable) {
        this.fileTable = fileTable;
    }

    public MyIStack<Stmt> getExecStack() {
        return execStack;
    }

    public MyIMap<String, Integer> getSymbolTable() {
        return symbolTable;
    }

    public MyIList<Integer> getOutput() {
        return output;
    }

    public void setExecStack(MyIStack<Stmt> execStack) {
        this.execStack = execStack;
    }

    public void setSymbolTable(MyIMap<String, Integer> symbolTable) {
        this.symbolTable = symbolTable;
    }

    public void setOutput(MyIList<Integer> output) {
        this.output = output;
    }

    public IHeap<Integer, Integer> getHeap() {
        return heap;
    }

    public void setHeap(IHeap<Integer, Integer> heap) {
        this.heap = heap;
    }

    public boolean isNotCompleted() {
        return !execStack.isEmpty();
    }

    public PrgState oneStep() throws ModelException {
        if (execStack.isEmpty())
            throw new ModelException("the program finished executing");
        Stmt crtStmt = execStack.pop();
        return crtStmt.execute(this);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "\nID:" + id +
                "\nExeStack:" + execStack +
                "\nSymTable:" + symbolTable +
                "\nOut:" + output +
                "\nFileTable:" + fileTable +
                "\nHeap:" + heap;
    }
}
