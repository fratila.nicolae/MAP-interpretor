package model;

import exception.StackModelException;

public interface MyIStack<T> {
    T pop() throws StackModelException;
    void push(T el);
    boolean isEmpty();
    Iterable<T> getAll();
}
