package model.container;

import exception.MapModelException;
import model.MyIMap;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MyMap<K, V> implements MyIMap<K, V> {

    @Override
    public boolean contains(K key) {
        return map.containsKey(key);
    }

    private Map<K, V> map;

    public MyMap() {
        map = new HashMap<>();
    }

    private MyMap(Map<K,V> map){
        this.map=map;
    }

    @Override
    public V get(K key) throws MapModelException {
        if (!map.containsKey(key))
            throw new MapModelException("Map: Invalid key.");
        return map.get(key);
    }


    @Override
    public void put(K key, V value) {
        map.put(key,value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<K,V> e : map.entrySet()){
            sb.append('\n');
            sb.append(e.getKey());
            sb.append("-->");
            sb.append(e.getValue());
        }
        return sb.toString();
    }

    @Override
    public Iterable<K> getKeys() {
        return map.keySet();
    }

    @Override
    public Collection<V> getValues() {
        return map.values();
    }

    @Override
    public MyIMap<K, V> getCopy() {
        return new MyMap<K,V>(new HashMap<K,V>(map));
    }
}
