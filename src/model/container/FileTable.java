package model.container;

import model.FileITable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FileTable<K,V> implements FileITable<K,V> {

    private Map<K,V> map = new HashMap<>();

    @Override
    public void add(K key, V value) {
        map.put(key,value);
    }

    @Override
    public void remove(K key) {
        map.remove(key);
    }

    @Override
    public V find(K key) {
        return map.get(key);
    }

    @Override
    public boolean contains(K key) {
        return map.containsKey(key);
    }

    @Override
    public Iterable<K> getAll() {
        return map.keySet();
    }

    @Override
    public Collection<V> getValues() {
        return map.values();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<K,V> e : map.entrySet()){
            sb.append('\n');
            sb.append(e.getKey());
            sb.append("-->");
            sb.append(e.getValue());
        }
        return sb.toString();
    }
}
