package model.container;

import model.MyIList;

import java.util.ArrayList;
import java.util.List;

public class MyList<T> implements MyIList<T> {
    private List<T> list;

    public MyList() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(T element) {
        list.add(element);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (T i : list){
            sb.append('\n');
            sb.append(i);
        }
        return sb.toString();
    }

    @Override
    public Iterable<T> getAll() {
        return list;
    }
}
