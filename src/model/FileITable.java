package model;

import java.util.Collection;

public interface FileITable<K,V> {
    void add(K key, V value);
    void remove(K key);
    V find(K key);
    boolean contains(K key);
    Iterable<K> getAll();
    Collection<V> getValues();
}
